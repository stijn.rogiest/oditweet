module.exports = {
    purge: [],
    // purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
    darkMode: false, // or 'media' or 'class'
    theme: {
        extend: {
            disabled: ["opacity"],
        },
    },
    variants: {
        extend: {
            disabled: ["opacity"],
        },
    },
    plugins: [],
};
