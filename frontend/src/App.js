import { useState, useEffect, useRef } from "react";
import io from "socket.io-client";

const SERVER_URL = "http://10.108.167.112:3001";

const client = io(SERVER_URL);
const audioNotify = new Audio("/notify.mp3");

function Post({ post, onHeart }) {
    const previousHeartsRef = useRef(post.hearts);
    return (
        <div className="p-4 shadow-md rounded-lg border my-2 bg-white new-message-animation">
            <div className="flex flex-row flex-wrap items-center">
                <h2 className="text-blue-600 font-bold">{post.name}</h2>
                <p className="text-xs text-gray-400 ml-auto">{new Date(post.date).toLocaleString()}</p>
            </div>
            <p className="text-gray-500">{post.content}</p>
            <button
                className={"font-bold text-red-200 text-sm"}
                onClick={onHeart}
                ref={(buttonRef) => {
                    if (buttonRef && post.hearts !== previousHeartsRef.current) {
                        previousHeartsRef.current = post.hearts;
                        buttonRef.classList.remove("upvote-animation");
                        setTimeout(() => {
                            buttonRef.classList.add("upvote-animation");
                        }, 5);
                    }
                }}>
                {post.hearts} upvotes
            </button>
        </div>
    );
}

function App() {
    const [posts, setPosts] = useState([]);
    const [content, setContent] = useState();
    const [name, setName] = useState("");

    useEffect(() => {
        async function getPosts() {
            let res = await fetch(SERVER_URL + "/post");
            if (res.ok) {
                setPosts(await res.json());
            }
        }

        function onPost(post) {
            console.log("new post", post);
            audioNotify.currentTime = 0;
            audioNotify.play();
            setPosts((posts) => [post, ...posts]);
        }

        function onHeart(id, hearts) {
            console.log("onHeart", id);
            setPosts((posts) => {
                let newPosts = [...posts];

                let index = newPosts.findIndex((e) => e.id == id);
                if (index >= 0) {
                    newPosts[index].hearts = hearts;
                }

                return newPosts;
            });
        }

        function onClear() {
            setPosts([]);
        }

        client.on("post", onPost);
        client.on("heart", onHeart);
        client.on("clear", onClear);

        getPosts();

        if (localStorage.getItem("name")) {
            setName(localStorage.getItem("name"));
        } else {
            let n = (window.prompt("What is your name?") || "user" + Math.random()).slice(0, 30);
            localStorage.setItem("name", n);
            setName(n);
        }

        return () => {
            client.off("heart", onHeart);
            client.off("post", onPost);
            client.off("clear", onClear);
        };
    }, []);

    return (
        <div className="flex justify-center p-5 min-h-screen bg-gray-100">
            <div>
                <form
                    className="flex flex-col items-end"
                    onSubmit={async (ev) => {
                        ev.preventDefault();
                        if (!content.trim()) return;
                        client.emit("post", { name: name, content: content.trim() });
                        setContent("");
                    }}>
                    <input
                        className="border-2 p-2 w-full rounded-lg appearance-none border-blue-600"
                        value={content}
                        onChange={(ev) => setContent(ev.target.value)}
                    />
                    <button
                        disabled={!content}
                        className={"py-2 px-4 transition text-white rounded-lg mt-2 " + (content ? "bg-blue-600 hover:bg-blue-300" : "bg-gray-300")}>
                        Send
                    </button>
                </form>

                <div className="w-96 my-2">
                    {posts.map((e) => (
                        <Post key={e.id} post={e} onHeart={() => client.emit("heart", e.id)} />
                    ))}
                </div>
                {!posts.length && <div className="text-gray-500 p-6 border rounded-lg">No posts yet!</div>}
            </div>
        </div>
    );
}

export default App;
