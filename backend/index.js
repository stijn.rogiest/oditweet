const express = require("express");
const cors = require("cors");
const app = express();
const http = require("http");
const server = http.createServer(app);
const io = require("socket.io");

let socketServer = io(server, { cors: { origin: "*" } });
let postId = 1;
let posts = [];

app.use(cors());
app.use(express.json());

app.get("/post", (req, res) => {
    res.json(posts);
});

app.get("/clear", (req, res) => {
    posts = [];
    socketServer.emit("clear");
    res.end("history cleared");
});

server.listen(3001, () => {
    console.log("listening on *:3001");
});

socketServer.on("connection", (socket) => {
    console.log("new connection");

    socket.on("post", (postData) => {
        let post = { hearts: 0, ...postData, id: postId++, date: new Date().getTime() };
        posts.unshift(post);
        socketServer.emit("post", post);
    });

    socket.on("heart", (id) => {
        let postIndex = posts.findIndex((e) => e.id == id);
        if (postIndex >= 0) {
            posts[postIndex].hearts++;
            socketServer.emit("heart", id, posts[postIndex].hearts);
        }
    });
});
